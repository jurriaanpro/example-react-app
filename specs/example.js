describe('my webdriverio tests', function () {
    it('should have correct title', function () {
        browser.url('/');
        browser.getTitle().should.be.equal('React App');

        console.log(browser.getTitle());
    });
});
